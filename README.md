Firebase_POC
=========

## Build

### Local
1. `git clone git@bitbucket.org:vibhor89/firebase_poc.git`
2. `npm install`
3. `bower install`
4. `grunt && grunt serve`

### QA:

1. `npm install -g firebase-tools`
2. `grunt build` -> used to create minified vesrion of app directory(to minified html css & jS files ).
3. `firebase init` -> used to create the firebase.json, ask for public directory(dist for QA) -> It can skipped, as we already have one in the code base.
4. `firebase deploy` -> to deploy the application. (`https://connect-health.firebaseapp.com`)

==========
