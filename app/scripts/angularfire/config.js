angular.module('firebase.config', [])
  .constant('FBURL', 'https://connect-health.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['password'])

  .constant('loginRedirectPath', '/login');
