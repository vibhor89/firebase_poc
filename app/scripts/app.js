'use strict';

/**
 * @ngdoc overview
 * @name firebasePocApp
 * @description
 * # firebasePocApp
 *
 * Main module of the application.
 */
angular.module('firebasePocApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.utils',
    'simpleLogin'
  ]);
