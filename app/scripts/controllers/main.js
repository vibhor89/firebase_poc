'use strict';

/**
 * @ngdoc function
 * @name firebasePocApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the firebasePocApp
 */
angular.module('firebasePocApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
